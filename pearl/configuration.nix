# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      #./home.nix
    ];

  environment.etc."machine-id".source = "/nix/persist/etc/machine-id";

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "pearl";
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Prague";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkb.options in tty.
  # };

  # Enable the X11 windowing system.
  services.xserver.enable = true;


  # Sway environment
  services.xserver.displayManager.gdm.enable = true; 
  #services.xserver.displayManager.gdm.wayland = false; 
  services.xserver.desktopManager.gnome.enable = true; 

  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true; # gtk will work properly
    extraPackages = with pkgs; [
      swaylock
      swayidle
      wl-clipboard
      gcc
      gdb
      #wl-recorder
      mako
      grim
      slurp
      alacritty
      wofi
    ];
    extraSessionCommands = ''
      export SDL_VIDEODRIVER=wayland
      export QT_QPA_PLATFORM=wayland
      export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
      export _JAVA_AWT_WM_NOREPARENTING=1
      export MOZ_ENABLE_WAYLAND=1
   '';
  };

  programs.waybar.enable = true;

  # Graphics settings
  services.xserver.videoDrivers = [ "nouveau" ];
  hardware.opengl.driSupport32Bit = true;

  # Configure keymap in X11
  # services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable= true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };
  hardware.pulseaudio.enable=false;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = false;
   users.users.pethod = {
     isNormalUser = true;
     extraGroups = [ "wheel" "libvirtd" "docker" "audio" "video" "input" "systemd-journal" "networkmanager" "network" ];
     initialHashedPassword = "$6$wlI9HF//RfxVIiAX$YcKCSATiuAbqUlk4km6UhMp6MXEUstedm8Pmt5nXlAcVNdALhh0l5G9Alv1je7wiKylsVa0FDbMstDui5Nw2S0";
     packages = with pkgs; [
       vim
       firefox
       tree
     ];
   };

  services.udev.extraRules = ''
ACTION=="add", SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTR{idVendor}=="4255", GROUP="input", MODE="0666"
SUBSYSTEM=="usb", ATTR{idVendor}=="16c0",ATTR{idProduct}=="05df", MODE="0666"
KERNEL=="hidraw*",  ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="05df", MODE="0660", GROUP="input", TAG+="systemd" ENV{SYSTEMD_WANTS}="usbrelayd.service"
KERNEL=="hidraw*",  ATTRS{idVendor}=="0519", ATTRS{idProduct}=="2018", MODE="0660", GROUP="input", SYMLINK+="usbrelay%b"

SUBSYSTEM=="usb", ATTR{idVendor}=="5131",ATTR{idProduct}=="2007", MODE="0666"
KERNEL=="hidraw*",  ATTRS{idVendor}=="5131", ATTRS{idProduct}=="2007", MODE="0660", GROUP="input", SYMLINK+="usbrelay%b"
  '';

  # List packages installed in system profile. To search, run:
  # $ nix search wget
   environment.systemPackages = with pkgs; [
(pass.withExtensions (exts: [
    exts.pass-otp
  ]))
     vim
     usbrelay
     usbutils
     git
     tig
   ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  services.udev.packages = [ pkgs.yubikey-personalization ];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # Virtualization
  programs.dconf.enable = true;
  services.qemuGuest.enable = true;
  boot.kernelModules = [ "kvm-intel" ];

  virtualisation.libvirtd = {
    enable = true;
    qemu.ovmf.enable = true;
    onBoot = "ignore";
    onShutdown = "shutdown";
  };
  virtualisation.libvirtd.qemu = {
      runAsRoot = true;
  };

  virtualisation.docker.enable = true;
  programs.virt-manager.enable = true;
  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

