{ config, pkgs, ... }:
let 
  hostname = "dungeon";
  wgIP = "10.100.0.1";
  grps = [];
in
{
  imports = [
    ./configuration-hardware.nix
    ./load-secrets.nix
    (import ./nixos/networking.nix { inherit hostname config wgIP; })
    ./nixos/wireguard.nix
    ./nixos/ssh.nix
    ./nixos/common.nix
    (import ./nixos/users.nix { inherit config pkgs grps; } )
  ];

  systemd.extraConfig = ''
    DefaultTimeoutStartSec=900s
  '';

  environment.systemPackages = with pkgs; [ 
    wireguard
    mosh
    tmux
    vim
    fish
    ncat
    tcpdump
    ];
}
