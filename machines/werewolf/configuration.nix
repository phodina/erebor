{ config, lib, pkgs, ... }:

let
    wgIP = "10.100.0.7";
    hostname = "werewolf";
    grps = [];
in {
    imports = [
      ./configuration-hardware.nix
      ./load-secrets.nix
      (import ./nixos/networking.nix { inherit hostname config wgIP; })
      ./nixos/common.nix
      ./nixos/ssh.nix
      ./nixos/nix.nix
      (import ./nixos/users.nix { inherit config pkgs grps; } )
     ];
}

