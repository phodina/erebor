{ config, lib, pkgs, ... }:

let 
  wgIP = "10.100.0.2";
  hostname = "anarchy";
  ykpam = false;
  grps = [ "video" "audio" "proc" "networkmanager" ]; 
in {

  imports = [
    ./configuration-hardware.nix
    ./load-secrets.nix
    ./nixos/laptop.nix
#    ./nixos/persistance.nix
    ./nixos/common.nix
    ./nixos/nix.nix
    (import ./nixos/users.nix { inherit config pkgs grps; } )
    (import ./nixos/networking.nix { inherit hostname config wgIP; })
    ./nixos/xserver.nix
    (import ./nixos/yubikey.nix { inherit config pkgs ykpam; })
  ];
}
