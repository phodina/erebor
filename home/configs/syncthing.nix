{ config, pkgs, lib, ... }:

let
  #addProxyPass = config.services.nginx.virtualHosts."${config.networking.hostName}".forceSSL;

in {
  services.syncthing = {
    enable = true;
    #dataDir = "/home/cylon2p0/";
    #openDefaultPorts = true;
#    user = "cylon2p0";
    #guiAddress = lib.mkIf addProxyPass "${config.networking.hostName}:8384";
  };

  #boot.kernel.sysctl."fs.inotify.max_user_watches" = 204800;

  #services.nginx.virtualHosts."${config.networking.hostName}".locations = lib.mkIf addProxyPass {
  #  "/syncthing/".proxyPass = "http://${config.networking.hostName}:8384/";
  #};
}
