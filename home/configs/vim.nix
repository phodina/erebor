{config, pkgs, ...}:

{
   programs.vim = {
      enable = true;
      settings = {
	 ignorecase = true;
         relativenumber = true;
         number = true;
      };
      plugins = with pkgs.vimPlugins; [
        vim-airline
        ];
	extraConfig = ''
set mouse=a
set wrap

" Set Leader key to space
nnoremap <SPACE> <Nop>
let mapleader=" "

noremap <Leader>W :w !sudo tee % > /dev/null

noremap! <F3> <C-R>=strftime('%c')<CR>

" Apply only for vimdiff
if &diff
autocmd BufWrite *[a-zA-Z0-9_.]* mkview               autocmd BufRead *[a-zA-Z0-9_.]* silent! loadview   
syn off
set cursorline
nnoremap <Leader>j ']c'
nnoremap <Leader>k '[c'
nnoremap <Leader>1 :diffg LO<CR>
nnoremap <Leader>2 :diffg BA<CR>
nnoremap <Leader>3 :diffg RE<CR>
endif

" Set hybrid numbers
set number relativenumber
set nu rnu

" Toggle between absolute & hybrid numbers
augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END
'';
};
    }
