{ pkgs, ... }:

{
  programs.alacritty = {
    enable = true;

    settings = {
      env = {
        "TERM" = "alacritty-direct";
      };

      window = {
        padding = {
          x = 5;
          y = 5;
        };
        dimensions = {
          lines = 75;
          columns = 100;
        };
      };

      font = {
        size = 12.0;
        use_thin_strokes = true;
      };

      background_opacity = 0.0;

      colors = {
        primary = {
          background = "0x282a36";
          foreground = "0xf8f8f2";
        };
        cursor = {
          style  = "Beam";
          text   = "0xFF261E";
          #cursor = "0xFF261E";
        };
        normal = {
          black   = "0x000000";
          red     = "0xFF5555";
          green   = "0x50FA7B";
          yellow  = "0xF1FA8C";
          blue    = "0xCAA9FA";
          magenta = "0xFF79C6";
          cyan    = "0x8BE9FD";
          white   = "0xBFBFBF";
        };
        bright = {
          black   = "0x575B70";
          red     = "0xFF6E67";
          green   = "0x5AF78E";
          yellow  = "0xF4F99D";
          blue    = "0xCAA9FA";
          magenta = "0xFF92D0";
          cyan    = "0x9AEDFE";
          white   = "0xE6E6E6";
        };
      };
    };
  };
}
