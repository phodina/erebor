{pkgs, config, ...}:
{
  programs.firefox-addons = with pkgs.firefox-addons; [
     # cookie-autodelete
     # decentraleyes
     # greasemonkey
      https-everywhere
     # link-cleaner
     # octotree
     # privacy-badger
     # reddit-enhancement-suite
     # save-page-we
     # stylus
     # swedish-dictionary
      ublock-origin
  ];
}
