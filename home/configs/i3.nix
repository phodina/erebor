{ config, pkgs, lib, ... }:
let
 screenshots= "~/Pictures/screenshots";
in
{
  xsession.windowManager.i3 = {
    enable = true;
    package = pkgs.i3-gaps;
    
    config = rec {
      modifier = "Mod4";
      
      # Hide window title#
      fonts = [ "DejaVu Sans Mono, FontAwesome 0" ];
      window.border = 0;
      
      bars = [
            {
              position = "top";
              statusCommand = "${pkgs.i3status-rust}/bin/i3status-rs ~/.config/i3status-rust/config.toml";
            }];

      gaps = {
        inner = 5;
        outer = 5;
      };
      
      keybindings = lib.mkOptionDefault {
        # Multimedia
	"${modifier}+F9" = "exec ./.config/i3/toggle_sink.sh";

	"Print" = "exec ${pkgs.flameshot}/bin/flameshot full -p ${screenshots}";
	"Shift+Print" = "exec ${pkgs.flameshot}/bin/flameshot gui -p ${screenshots}";
        "XF86AudioMute" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ toggle";
        "XF86AudioLowerVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ -5%";
        "XF86AudioRaiseVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ +5%";
        "XF86MonBrightnessDown" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set 5%-";
        "XF86MonBrightnessUp" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set +5%";

        # Control
        "${modifier}+Return" = "exec ${pkgs.alacritty}/bin/alacritty";
	"${modifier}+Shift+Return" = "exec ${pkgs.i3lock-fancy-rapid}/bin/i3lock-fancy-rapid 5 3";
        "${modifier}+d" = "exec ${pkgs.rofi}/bin/rofi -modi drun -show drun";
        "${modifier}+Shift+d" = "exec ${pkgs.rofi}/bin/rofi -show window";
        "${modifier}+g" = "exec ${pkgs.firefox}/bin/firefox";
	"${modifier}+a" = "exec ${pkgs.arandr}/bin/arandr";
	"${modifier}+z" = "exec ${pkgs.clipmenu}/bin/clipmenu";
        "${modifier}+Shift+x" = "exec systemctl suspend";

        # Focus
        "${modifier}+h" = "focus left";
        "${modifier}+j" = "focus down";
        "${modifier}+k" = "focus up";
        "${modifier}+l" = "focus right";

        # Move
        "${modifier}+Shift+h" = "move left";
        "${modifier}+Shift+j" = "move down";
        "${modifier}+Shift+k" = "move up";
        "${modifier}+Shift+l" = "move right";

        # Scratchpad
        "${modifier}+m" = "move scratchpad";
        "${modifier}+o" = "scratchpad show";

        # My multi monitor setup
	"${modifier}+b"       =  "exec xrandr --output HDMI-1 --off";
	"${modifier}+Shift+b" =  "exec xrandr --output HDMI-1 --auto --left-of DP-1";
        "${modifier}+n"       =  "move workspace to output DP-1";
        "${modifier}+Shift+n" =  "move workspace to output HDMI-1";
      };

      
      startup = [
        {
          command = "exec CM_LAUNCHER=rofi ${pkgs.clipmenu}/bin/clipmenud";
          always = true;
          notification = false;
        }
        {
          command = "exec ${pkgs.notify-osd}/bin/notify-osd";
          always = true;
          notification = false;
        }
        {
          command = "${pkgs.feh}/bin/feh --bg-scale ~/background.jpg";
          always = true;
          notification = false;
        }
      ];
    };
  };
}

