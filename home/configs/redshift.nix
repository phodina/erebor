{ config, pkgs, callPackage, ... }:

{
  services.redshift = {
    enable = true;
    brightness = {
      day = "1";
      night = "1";
    };
    temperature = {
      day = 5500;
      night = 3700;
    };
    latitude = "50.08804";
    longitude = "14.42076";
  };
}

