{ config, pkgs, lib, ... }:

{
  imports = [
      ./i3.nix
      ./git.nix
      #./i3status-rust.nix
      ./vim.nix
      ./rofi.nix
      ./alacritty.nix
      ./compton.nix
      ./redshift.nix
      ./syncthing.nix
      ./protonmail-bridge.nix
      ./emacs.nix
      #./firefox.nix
  ];

#  programs.emacs.enable = true;
#   home.file.".emacs.d" = {
#    # don't make the directory read only so that impure melpa can still happen
#    # for now
#    recursive = true;
#    source = pkgs.fetchFromGitHub {
#      owner = "syl20bnr";
#      repo = "spacemacs";
#      rev = "26b8fe0c317915b622825877eb5e5bdae88fb2b2";
#      sha256 = "00cfm6caaz85rwlrbs8rm2878wgnph6342i9688w4dji3dgyz3rz";
#      };
#    };

  services.protonmail-bridge = {
    enable = true;
    nonInteractive = true;
  };

  programs.fish.shellInit = ''
    set -x GPG_TTY=(tty)
    gpg-connect-agent /bye
    set -x SSH_AUTH_SOCK=(gpgconf --list-dirs agent-ssh-socket)
  '';

  home.sessionVariables = {
    EDITOR = "vim";
  };

  programs = {
    home-manager.enable = true;
    command-not-found.enable = true;
  };
  
  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
  };

  services.udiskie = {
    enable = true;
  };

  xsession.enable = true;
}
