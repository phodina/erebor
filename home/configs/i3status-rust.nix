{config, pkgs, libs, ...}:
{
 programs."i3status-rust" = {
   enable = true;
   icons = "awesome5";
   theme = "solarized-dark"; 
   bars = [
     {
     block = "disk_space";
     path = "/";
     alias = "/";
     info_type = "available";
     unit = "GB";
     interval = 60;
     warning = 20.0;
     alert = 10.0;
     }
     {
     block = "sound";
     format = "{output_name} {volume}%";
     on_click = "pavucontrol --tab=3";
     mappings = {
       "alsa_output.pci-0000_00_1f.3.analog-stereo" = "";
       "bluez_sink.70_26_05_DA_27_A4.a2dp_sink" = "";
       };
     }
   ];
 };

}
