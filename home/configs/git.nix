{config,...}:
{
  programs.git = {
    enable = true;
    aliases = {
      co = "checkout";
      cm = "commit -m";
      s = "status";
      lg =
        "log --all --decorate --color --graph --pretty=format:'%Cred%h%Creset %Cgreen(%cr)%Creset - %s %C(bold blue)<%an>[%G?]%Creset%C(auto)%d%Creset' --abbrev-commit";
      ll = ''
        log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat'';
      undo = "reset HEAD~1 --mixed";
    };

    delta.enable = true;

    extraConfig = {
      apply.whitespace = "fix";
      branch.autosetuprebase = "always";
      core.editor = "vim";
      core.whitespace = "fix,-indent-with-non-tab,trailing-space,cr-at-eol";
      diff.tool = "vimdiff";
      merge.tool = "vimdiff";
      merge.conflictstyle = "diff3";
      mergetool.prompt = "false";
      mergetool.keepBackup = "false";
      init.defaultBranch = "master";
      merge.renamelimit = "4096";
      pull.rebase = "false";
      push.default = "upstream";
      submodule.recurse = "true";
      tag.sort = "version:refname";
      protocol.keybase.allow = "allow";
    };

    ignores = [
      "*~"
      "*.swp"
    ];

    signing = {
      key = "85ACA061E93D5A13";
      signByDefault = true;
    };

    userEmail = "phodina@protonmail.com";
    userName = "Petr Hodina";
  };

}
