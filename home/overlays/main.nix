self: super: {
  #i3status-rust = i3status-rust.overrideAttrs (old: rec {src = builtins.fetchTarball "https://github.com/greshake/i3status-rust/archive/v0.14.7.tar.gz"; cargoSha256="0000000000000000000000000000000000000000000000000000";});

# Example how to fetch newer version of pkg
#  neofetch = super.neofetch.overrideAttrs (oldAttrs: {
#          src = builtins.fetchTarball "https://github.com/dylanaraps/neofetch/archive/master.tar.gz";
#        });

#  i3status-rust = super.i3status-rust.overrideAttrs (old: rec { 
#      name = "i3status-rust-${version}";
#      version = "0.14.7";
#
##      nativeBuildInputs = [ super.breakpointHook ];
#      src = super.fetchFromGitHub {
#        owner = "greshake";
#        repo = "i3status-rust";
#        rev = "094cfdc13953b31edda8914f5e536e52b93deb38";
#        sha256 = "6JtCcR+NTx5pEuAA+F+5+do8DOaatE5VRXtGtLeVAuY=";
#      };
#
#      cargoDeps = old.cargoDeps.overrideAttrs (super.lib.const { 
#         name = "${name}-vendor.tar.gz";
#         inherit src;
#         outputHash = "SmXgXU2lqeB1OT//Jm7q9XM62czExY5q6oXekSIux6Q=";
#      });});


# New pkg
#  ls-colors = self.callPackage ./pkgs/ls-colors.nix {runCommand = ???};

}
