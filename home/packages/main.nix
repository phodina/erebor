{pkgs,  ... }:
let
      gplayclip = with pkgs.python38Packages; (callPackage ./gplaycli.nix {   });
in
with pkgs;
{
    home.packages = with pkgs; [
      # MISC
      pass-otp protonvpn-cli wireshark-qt
      cachix arandr ffmpeg-full rofi rofi-pass i3status-rust
      # TERMINAL
      gotop htop zip unzip gnupg file fzf nmap
      imagemagick bat imv lsof clipmenu cntr jq
      shellcheck tcpdump transmission lorri usbutils
      # DEVELOPMENT
      rustup direnv docker hugo
      # OFFICE
      brave libreoffice-fresh pandoc p7zip
      texlive.combined.scheme-full texmaker
      (callPackage ./compton-unstable.nix { })
       gplayclip zathura
      #(callPackage ./firefox-addons.nix { })
      i3lock-fancy-rapid notify-desktop notify-osd
      # DEFAULT
      gimp brightnessctl prusa-slicer inkscape flameshot feh pavucontrol pirate-get mako
    ];
}
