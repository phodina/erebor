{stdenv, fetchFromGitLab, haskellPackages.cabal-install}:

stdenv.mkDerivation{
  pname = "firefox-addons";
  version="2019-12-20";

  src = fetchFromGitLab {
    owner = "rycee";
    repo = "nixpkgs-firefox-addons";
    rev = "44f73a5c5fb1d753fe11eed44e729f7ed707cb65";
    sha256 = "2acbc20b00141b8fbf0ed0c8685df2341d2ddca7aecd512c0b0a1da12f61bf92";
    };

  nativeBuildInputs = [ haskellPackages.cabal-install ];
  buildInputs = [];
}
