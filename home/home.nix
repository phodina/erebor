{ config, pkgs, lib, ... }:

{
  imports = [
      ./configs/main.nix
      ./packages/main.nix
    ];
  nixpkgs.overlays = [ (import ./overlays/main.nix) ];

  services.lorri.enable = true;

  home.file.i3status = {
      source = ./.config/i3status-rust/config.toml;
      target = ".config/i3status-rust/config.toml";
  };

  home.keyboard = {
    layout = "us, cz";
    variant = ",qwerty";
    options = [ "grp:alt_shift_toggle" "terminate:ctrl_alt_bksp" ];
    };

  home.file.toggle_sink = {
      executable = true;
      source = ./.config/i3/toggle_sink.sh;
      target = ".config/i3/toggle_sink.sh";
  };

#  programs.emacs.enable = true;
#  home.file.".emacs.d" = {
#    source = builtins.fetchGit {
#        url = "https://github.com/syl20bnr/spacemacs";
#        ref = "develop";
#        };
#    recursive = true;
#    };
}

