{ config, options, lib, ... }: {
  imports = [
    <nixpkgs/nixos/modules/installer/sd-card/sd-image-aarch64-installer.nix>
    ./configuration.nix
  ];

  config.sdImage.firmwareSize = 512;
  # bzip2 compression takes loads of time with emulation, skip it.
  config.sdImage.compressImage = false;
}
