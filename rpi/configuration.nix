{ config, pkgs, lib, ... }: 
let
  wgIP="10.0.0.50";
  hostname = "blueberry";
  grps = [ ];
in
{
#  nixpkgs.overlays = [ (import ./overlay.nix) ];  
  imports = [
    ./configuration-hardware.nix
    (import ./nixos/users.nix { inherit config pkgs grps; } )
    (import ./nixos/networking.nix { inherit hostname config wgIP; })
    ./nixos/ssh.nix
    ];
  
  networking.wireless.enable = false;

  environment.systemPackages = with pkgs; [
    libraspberrypi
    wireguard
    tmux
    fish
    vim
    ]; 

  # Preserve space by sacrificing documentation and history
  documentation.nixos.enable = false;
  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 30d";
}
