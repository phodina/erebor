# Erebor
![Erebor](erebor.png)
Declarative configuration for NixOS.

```
sudo ./populate.sh <MACHINE>
sudo nixos-rebuild build
```

## Yubikey U2F PAM
Please run this before enabling Yubikey!!!
```
nix-env -iA nixos.pam nixos.pam_u2f
mkdir ~/.config/Yubico
pamu2fcfg > ~/.config/Yubico/u2f_keys
```

## Cleanup
```
sudo mkdir /mnt
sudo mount -o subvol=/ /dev/mapper/lvm-root /mnt
./taint.sh
```
