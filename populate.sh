#!/bin/sh

set -e

DEST=/etc/nixos/
SRCS=""
ROOT=$(git rev-parse --show-toplevel)

if [ ! -d "${ROOT}" ] ;then
	echo "Not in git repo!"
	exit 1
fi

if [ -z "$1" ] ;then
	echo "Please specify machine!"
	exit 1
fi

HW_NIX=${ROOT}/machines/$1/

if [ ! -e "${HW_NIX}" ]; then
	echo "Such machine does not exist"
	exit
fi

sudo cp -r ${HW_NIX}/* ${ROOT}/load-secrets.nix ${ROOT}/nixos ${DEST}
