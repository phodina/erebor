{config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    home-manager
    redshift
    networkmanager
    firefox
    openconnect
  ];

  powerManagement.powertop.enable = true;
  services.tlp.enable = true;

  # Enable sound
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.video.hidpi.enable = true;

  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
      ];
    };
}
