{config, wgIP, hostname, ...}:
let
  wgPort = 60990;
  dungeon = {
    publicKey = "Zc4oXOUTuJmi4jVoh3DYlQfZZs6J+TfL17z5y53K+TI=";
    allowedIPs = [ "10.100.0.0/24" ];
    endpoint = "37.205.12.244:60990";
    persistentKeepalive = 25;
    };
in
{
  networking.wireguard.interfaces = {
    wg0 = {
      ips = [ wgIP ];
      listenPort = wgPort;
      privateKeyFile = "/etc/wg-priv";
      peers = [ dungeon ];
    };
  };

  # Networking
  networking = {
  	hostName = hostname;
  	networkmanager.enable = true;
  	nameservers = ["127.0.0.1" "::1"];
  	networkmanager.dns = "none";
  	firewall.allowedTCPPorts = [ 80 443 ];
  	firewall.allowedUDPPorts = [ wgPort ];
  	extraHosts = ''
10.100.0.1 dungeon
10.100.0.2 anarchy
10.100.0.3 pearl
10.100.0.7 werewolf
10.100.0.10 router-prg
10.100.0.50 blueberry
10.100.0.51 blackberry
'';
  };

  services.dnscrypt-proxy2 = {
    enable = true;
    settings = {
      ipv6_servers = true;
      require_dnssec = true;
      dnscrypt_servers = true;
      doh_servers = true;

      blacklist = {
        blacklist_file = "/etc/blacklist.txt";
	};

      sources.public-resolvers = {
        urls = [
          "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
          "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
        ];
        cache_file = "/var/lib/dnscrypt-proxy2/public-resolvers.md";
        minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      };

	#server_name = [ "193.17.47.1" "185.43.135.1" "2001:148f:ffff::1" "2001:148f:fffe::1" ];
    };
  };

  systemd.services.dnscrypt-proxy2.serviceConfig = {
    StateDirectory = "dnscrypt-proxy2";
  };
}
