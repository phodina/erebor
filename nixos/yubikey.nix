{config, pkgs, ykpam ? false, ... }:
{
  programs.ssh.startAgent = false;

  services.pcscd.enable = true;

  programs.gnupg = {
    agent.enable = true;
    agent.enableSSHSupport = true;
    agent.enableExtraSocket = true;
    agent.enableBrowserSocket = true;
    dirmngr.enable = true;
  };

  environment.systemPackages = with pkgs; [
    gnupg
    yubikey-personalization
  ];

  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];

  # Enable PAM
  security.pam = {
    u2f = {
      enable = ykpam;
      control = "required";
    };
    services = {
      #login.u2fAuth = true;
      lightdm.u2fAuth = true;
      i3lock-fancy-rapid.u2fAuth = true;
    };
  };

# # Lock computer when Yubikey is removed
# environment.etc.nixos."yubikey.sh" = {
#   text = ''
##!/bin/sh
#if [ -z "$(lsusb | grep Yubico)" ]; then
#  loginctl list-sessions | grep '^\ ' | awk '{print $1}' | xargs -i loginctl lock-session '{}'
#fi
#'';
#   mode = "0440";
#   };
#
#  services.udev.extraRules = ''
#ACTION=="remove", ENV{ID_VENDOR_ID}=="1050", ENV{ID_MODEL_ID}=="0407", RUN+="/usr/bin/lockscreen-all"
#'';
}
