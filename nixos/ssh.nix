{ config, lib, pkgs, ... }:

{
  services = {
    fail2ban = {
      daemonConfig = ''
        [sshd]
        maxretry = 3
        findtime = 43200
        bantime = 86400
      '';

      enable = lib.mkForce config.services.openssh.enable;
    };

    openssh = {
      allowSFTP = false;
      ports = [ 69 ];
      challengeResponseAuthentication = false;
      enable = true;

      extraConfig = ''
        ClientAliveInterval 300
        ClientAliveCountMax 2
      '';

      hostKeys = [
        {
          path = "/etc/ssh/ssh_host_ed25519_key";
          rounds = 127;
          type = "ed25519";
        }
        {
          bits = 4096;
          path = "/etc/ssh/ssh_host_rsa_key";
          type = "rsa";
        }
      ];

      kexAlgorithms = [
        "curve25519-sha256@libssh.org"
        "diffie-hellman-group14-sha256"
        "diffie-hellman-group16-sha512"
        "diffie-hellman-group18-sha512"
      ];

      macs = [
        "hmac-sha2-512-etm@openssh.com"
        "hmac-sha2-256-etm@openssh.com"
        "umac-128-etm@openssh.com"
      ];

      passwordAuthentication = false;
      permitRootLogin = "no";
    };
  };
}
