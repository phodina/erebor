{ config, pkgs, grps, ... }:

{
  #users.mutableUsers = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.cylon2p0 = {
    isNormalUser = true;
    extraGroups = [ "wheel" ] ++ grps;
    openssh.authorizedKeys.keys = [ ''ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDg/3m4MUAmsXyDrFcCtAMmg90NZwRIKMhhlk59V+CxdQy3IrKi5Xp3bqXr1hm0Z7nAFdBKEenK5nXSeuwzf3On43QAZoRbsm0/8J/5BZ+4nfpQ0iCZaSsbDR1eb6Qvcxnth5EeGJDNEaMDDCriM0v6lnDHCzJpIpMKC1cZd+bE4nEBhKQPxKs+/1DFV76GuTG0IQqHrq2HTQmm03zcuH/tg7if5tMWbJW1FXrZNyvtHl8VwdpRo4n8bLiIK8x8mJvnm6USCxmbI/M7nBvX5jwEJ1BOSzBRs+r7LRZ4bGneaoWBSyKgTMFAB9xGp1q0BUx26k7Gh2Bocl1A14oJIjnQ3QRmz+ZMIjtfObLdLaC5WoP6N++ienDOLH4Uxc8AmaJaBso9jseeaieGh3+NnfpqZ5QCN5J024zal0YCkvdebib1Xpy5MTnDaU9TRIQmLyNfyzyu9oD7GmW2HRbCBKFUWFUw/pbHtzSTJmZoj2+uEAVXqXu2adPD5jG3SXWAdELEKOtFK/PsgqLUPR6Gg/WCBfdKdER4XYgDyo+23TZ7LnbkToQY1xh+aQ9eNcHd/ArM3VBUl7ONU2sn36qS+1ELCr65YDVKDkUQm8S+OR098/RqvnDHnj3MhRgr4WteId8f3CHckpj8JA+UTc3a5r95HYDhglknk+JvYR+8DiqVKw== (none)'' ];
    shell = pkgs.fish;
    };

  fonts = {
    fonts = with pkgs; [
      dejavu_fonts
      emacs-all-the-icons-fonts
      font-awesome
      hasklig
      powerline-fonts
      ];
    fontconfig.defaultFonts.monospace = [ "DejaVu Sans Mono for Powerline" ];
  };
}
