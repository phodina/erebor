{ config, ...}:
let
  anarchy = { 
    publicKey = "wOpMEGCvBm7XcLXFLTCIzVFAsM6z2H/M30Idi5AiNGY=";
    allowedIPs = [ "10.100.0.2" ];
    persistentKeepalive = 25;
  };

  pearl = {
    publicKey = "WjcnkHvrRp55D/P6rQRurqgR1oDuXKDZC9cvZs39vFY=";
    allowedIPs = [ "10.100.0.3" ];
    persistentKeepalive = 25;
  };

  werewolf = {
   publicKey = "Ug3c/FXT7LFQ/Mz4RWjeu/ifuNsJo4nXAxCrxPxSVmI=";
   allowedIPs = [ "10.100.0.7" ];
   persistentKeepalive = 25;
  };

  router-prg = {
    publicKey = "CkIsVR1vTAGqSTX+51WxTeV51P1OWoQhQtJQHSNgbis=";
    allowedIPs = [ "10.100.0.10" ];
    persistentKeepalive = 25;
  };

  blueberry = {
    publicKey = "QdiqEkVyB+k0NRQK6bH4/RH1413x5v7MkdSLYMBUHkI=";
    allowedIPs = [ "10.100.0.50" ];
    persistentKeepalive = 25;
  };

in {
  networking.wireguard.interfaces = {
    wg0 = {
      peers = [
	anarchy
	pearl
	werewolf
	router-prg
	blueberry
      ];
    };
  };
}
