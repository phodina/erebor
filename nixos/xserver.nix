{config, libs, pkgs, ... }:

{
  services.xserver.enable = true;
  services.xserver.displayManager.defaultSession = "none+i3";
  services.xserver.windowManager.i3 = {
    enable = true;
    package = pkgs.i3-gaps;
    extraPackages = with pkgs; [
      i3status-rust
      i3lock-fancy-rapid
      arandr
      ];
  };

  services.xserver.videoDrivers = [ "modesetting" ];
  services.xserver.useGlamor = true;

  services.xserver.libinput = {
    enable = true;
    disableWhileTyping = true;
    naturalScrolling = true;
    accelSpeed = "0.5";
    };
}
