{ config, lib, pkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;

  programs.fish.enable = true;

  time.timeZone = "Europe/Prague";

  # List packages installed in system profile
  environment.systemPackages = with pkgs; [
    cntr
    bind
    tig
    skim
    ripgrep
    tmux
    fish
    wget
    git
    vim
    ranger
    wireguard
    networkmanager
    ];
}
