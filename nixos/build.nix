{config,...}:
{
  programs.ssh.extraConfig = ''
    Host remote_host
      ProxyCommand ssh -i /root/.ssh/my_key -W remote_host:1221 ubuntu@jump_host
      IdentityFile /root/.ssh/my_key
      User ubuntu
  '';

}
