#!/bin/sh

set -e

DRIVE=$1
if [ -z "${DRIVE}" ];then
	echo "Usage: $0 <DRIVE>"
	exit 1
fi



partition(){
	#sgdisk --zap-all "${DRIVE}"
	#sgdisk --clear --new=1:0:+512MiB --typecode=1:ef00 --change-name=1:EFI --new=2:0:0 --typecode=2:8E00 --change-name=2:cryptsystem "${DRIVE}"
	#sleep 3
	#sync
	#cryptsetup luksFormat --cipher=aes-xts-plain64 "${DRIVE}2"
	#cryptsetup luksOpen "${DRIVE}2" cryptsystem
	mkfs.btrfs -f /dev/mapper/cryptsystem
	mkfs.fat -F32 -n boot "${DRIVE}p1"
	sleep 3
	sync
	mount -t btrfs /dev/mapper/cryptsystem /mnt
	btrfs subvolume create /mnt/root
	btrfs subvolume create /mnt/home
	btrfs subvolume create /mnt/gnu
	btrfs subvolume create /mnt/persist
	btrfs subvolume create /mnt/log
	btrfs subvolume create /mnt/swap
	touch /mnt/swap/swapfile
	truncate -s 0 /mnt/swap/swapfile
	chattr +C /mnt/swap/swapfile
	fallocate -l 8G /mnt/swap/swapfile
	chmod 600 /mnt/swap/swapfile
	btrfs property set /mnt/swap/swapfile compression none
	btrfs subvolume snapshot -r /mnt/root /mnt/root-blank
	umount /mnt
}

mount_partitions (){
	mount -o subvol=root,compress=zstd,noatime /dev/mapper/cryptsystem /mnt
	mkdir /mnt/home
	mount -o subvol=home,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/home
	mkdir /mnt/gnu
	mount -o subvol=gnu,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/gnu
	mkdir /mnt/persist
	mount -o subvol=persist,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/persist
	mkdir -p /mnt/var/log
	mount -o subvol=log,compress=zstd,noatime /dev/mapper/cryptsystem /mnt/var/log
	mkdir -p /mnt/boot/efi
	mount "${DRIVE}p1" /mnt/boot/efi
}

partition
mount_partitions
